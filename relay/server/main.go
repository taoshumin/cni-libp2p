/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/libp2p/go-libp2p"
	"github.com/libp2p/go-libp2p-core/host"
	"github.com/libp2p/go-libp2p-core/network"
	"github.com/libp2p/go-libp2p-core/peer"
	swarm "github.com/libp2p/go-libp2p-swarm"
	"github.com/multiformats/go-multiaddr"
	"io/ioutil"
	"os"
	"os/signal"
	"time"
	"x6t.io/p2p"
)

func main() {
	ch := make(chan os.Signal, 1)
	hs, pi, err := NewHost("/ip4/127.0.0.1/tcp/30002/p2p/QmTQR2nesSNXTDDaqHqbHcaHZiqAzCv7bh9XvUkhVQkPy9")
	if err != nil {
		panic(err)
	}
	if err := Connect(hs, pi); err != nil {
		panic(err)
	}
	ConnectNode(hs,pi)
	signal.Notify(ch, os.Interrupt)
	<-ch
}

func MakePeerInfo(relay string) (*peer.AddrInfo, error) {
	addr, err := multiaddr.NewMultiaddr(relay)
	if err != nil {
		return nil, err
	}

	pid, err := addr.ValueForProtocol(multiaddr.P_P2P)
	if err != nil {
		return nil, err
	}

	rpid, err := peer.Decode(pid)
	if err != nil {
		return nil, err
	}

	rpaddr, _ := multiaddr.NewMultiaddr(fmt.Sprintf("/p2p/%s", pid))
	muAddr := addr.Decapsulate(rpaddr)
	return &peer.AddrInfo{
		ID:    rpid,
		Addrs: []multiaddr.Multiaddr{muAddr},
	}, nil
}

func NewHost(reply string) (host.Host, *peer.AddrInfo, error) {
	peerInfo, err := MakePeerInfo(reply)
	if err != nil {
		return nil, nil, err
	}

	hs, err := libp2p.New(
		libp2p.EnableRelay(),
		libp2p.EnableAutoRelay(),
		libp2p.ForceReachabilityPrivate(),
		libp2p.StaticRelays([]peer.AddrInfo{*peerInfo}),
		libp2p.EnableHolePunching(),
	)
	if err != nil {
		return nil, nil, err
	}
	fmt.Printf("self address %s  addr: %s\n",hs.Addrs(),hs.ID().String())
	return hs, peerInfo, nil
}

func Connect(hs host.Host, pi *peer.AddrInfo) error {
	if err := hs.Connect(context.Background(), *pi); err != nil {
		fmt.Printf("host connect err:%s\n", err)
		return err
	}

	fmt.Printf("current service has been connect relay service \n")
	// 连接上中继relay服务，发送消息告诉中继服务
	ss, err := hs.NewStream(context.Background(), pi.ID, p2p.Request)
	if err != nil {
		fmt.Printf("tell relay service connect err: %s\n", err)
		return err
	}

	// 向中继服务发送连接请求
	defer ss.Close()
	ss.Write([]byte("connect"))
	return nil
}

// ConnectNode 中继服务获取connect消息后，返回A,B服务节点信息
func ConnectNode(hs host.Host,pi *peer.AddrInfo) {
	hs.SetStreamHandler(p2p.Message, func(stream network.Stream) {
		buf, err := ioutil.ReadAll(stream)
		if err!=nil{
			fmt.Printf("p2p message stream err: %s\n",err)
			return
		}
		fmt.Printf("message body: %s \n",string(buf))
		stream.Close()
	})

	hs.SetStreamHandler(p2p.Response, func(stream network.Stream) {
		fmt.Printf("relay service recive connect,return node service information.\n")
		buf, err := ioutil.ReadAll(stream)
		if err != nil {
			fmt.Printf("peer decode err: %s \n", err)
			return
		}

		var objc p2p.Objects
		if err := json.Unmarshal(buf, &objc); err != nil {
			fmt.Printf("peer decode err: %s \n", err)
			return
		}

		if len(objc) <= 1 {
			fmt.Printf("object lenth <=1")
			return
		}

		otherObjs := make(p2p.Objects, 0, 1)
		for _, object := range objc {
			// 获取非当前节点信息
			if object.Pretty != hs.ID().Pretty() {
				otherObjs = append(otherObjs, object)
				break
			}
		}
		// 获取A,B两服务节点信息
		fmt.Printf("start connect relay server: %s \n", objc)
		ConnectAB(hs, otherObjs[0].Pretty, otherObjs[0].ID,pi)
	})
}

func ConnectAB(hs host.Host, nodePid, nodeAddr string,pi *peer.AddrInfo) {
	fmt.Printf("nodePid: %s nodeAddr: %s \n", nodePid, nodeAddr)

	peerID, err := peer.Decode(nodePid)
	if err != nil {
		fmt.Printf("peer decode err: %s \n", err)
		return
	}

	hs.Network().(*swarm.Swarm).Backoff().Clear(peerID)
	p2pAddress := nodeAddr + "/p2p/" + pi.ID.Pretty() + "/p2p-circuit/p2p/" + peerID.Pretty()

	targetAddr, err := multiaddr.NewMultiaddr(p2pAddress)
	if err != nil {
		fmt.Printf("multiaddr address:%s \n", err)
		return
	}

	targetAddrInfo := peer.AddrInfo{
		ID:    peerID,
		Addrs: []multiaddr.Multiaddr{targetAddr},
	}

	if err := hs.Connect(context.Background(), targetAddrInfo); err != nil {
		fmt.Printf("mmm host connect err:%s \n", err)
		return
	}

	ss, err := hs.NewStream(context.Background(), targetAddrInfo.ID, p2p.Message)
	if err != nil {
		fmt.Printf("mm new stream err: %s\n", err)
		return
	}
	defer ss.Close()

	time.Sleep(5*time.Second)  // 测试中继服务断开
	ss.Write([]byte(fmt.Sprintf("hello-%d, address: %s",10,targetAddrInfo.ID.Pretty())))
}
