/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"context"
	"fmt"
	"github.com/libp2p/go-libp2p"
	"github.com/libp2p/go-libp2p-core/host"
	"github.com/libp2p/go-libp2p-core/network"
	"github.com/libp2p/go-libp2p-core/peer"
	relayv1 "github.com/libp2p/go-libp2p/p2p/protocol/circuitv1/relay"
	"github.com/multiformats/go-multiaddr"
	"os"
	"os/signal"
	"x6t.io/p2p"
)

var peers = make(map[peer.ID]multiaddr.Multiaddr)

func main() {
	ch := make(chan os.Signal, 1)
	hs, err := NewHost(30002)
	if err != nil {
		panic(err)
	}
	Listen(hs)
	signal.Notify(ch, os.Interrupt)
	<-ch
}

func NewHost(port int) (host.Host, error) {
	addr, _ := multiaddr.NewMultiaddr(fmt.Sprintf("/ip4/0.0.0.0/tcp/%d", port))
	hs, err := libp2p.New(
		libp2p.ListenAddrs(addr),
		libp2p.DisableRelay(),
		libp2p.EnableHolePunching(),
	)
	if err != nil {
		return nil, err
	}
	_, err = relayv1.NewRelay(hs)
	if err != nil {
		return nil, err
	}
	return hs, nil
}

func Listen(hs host.Host) {
	fmt.Printf("/ip4/127.0.0.1/tcp/30002/p2p/%s \n", hs.ID().Pretty())

	hs.SetStreamHandler(p2p.Request, func(stream network.Stream) {
		pid := stream.Conn().RemotePeer()
		remotAddr := stream.Conn().RemoteMultiaddr()
		localAddr := stream.Conn().LocalMultiaddr()

		peers[pid] = remotAddr
		fmt.Printf("connect services pid: %s remoteAddr: %s localAddr: %s \n", pid, remotAddr, localAddr)

		// 测试，2个节点,A,B
		if len(peers) != 2 {
			return
		}

		// 获取A,B两节点信息
		objs := make(p2p.Objects, 0, 2)
		for k, v := range peers {
			objs = append(objs, p2p.Object{
				Pretty: k.Pretty(),  // hash
				ID:     v.String(), // address
			})
		}

		// 发送A节点数据发送给B，发送B节点数据给A
		for k, _ := range peers {
			fmt.Println("perr id:", k)
			s, err := hs.NewStream(context.Background(), k, p2p.Response)
			if err != nil {
				fmt.Printf("new stream err: %s \n", err)
				break
			}
			fmt.Printf("send peer objects: %s\n", objs)
			defer s.Close()
			s.Write([]byte(objs.Marshal()))
		}

		// Trunk
		peers = make(map[peer.ID]multiaddr.Multiaddr)
	})

	hs.SetStreamHandler(p2p.Response, func(stream network.Stream) {
		fmt.Printf("a new Stream relay response")
	})
}
