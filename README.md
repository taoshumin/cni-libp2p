## [libp2p](https://docs.libp2p.io/concepts/transport/)

[https://zfunnily.github.io/2021/10/gop2ptwo/](https://zfunnily.github.io/2021/10/gop2ptwo/)

##### [一、节点A与节点B (normal)](https://gitlab.com/taoshumin/cni-libp2p/-/tree/master/normal)
##### [二、内网A，内网B，中继C (relay)](https://gitlab.com/taoshumin/cni-libp2p/-/tree/master/relay)
##### [三、内网A内网B打洞不成功中继C，打洞成功A，B通讯 (nat)](https://gitlab.com/taoshumin/cni-libp2p/-/tree/master/nat)