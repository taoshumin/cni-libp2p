/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"context"
	"crypto/rand"
	"fmt"
	"github.com/libp2p/go-libp2p"
	"github.com/libp2p/go-libp2p-core/crypto"
	"github.com/libp2p/go-libp2p-core/host"
	"github.com/libp2p/go-libp2p-core/peer"
	"github.com/libp2p/go-libp2p-core/peerstore"
	ma "github.com/multiformats/go-multiaddr"
	"io/ioutil"
	"os"
	"os/signal"
	"x6t.io/p2p"
)

// A
func main() {
	ch := make(chan os.Signal, 1)
	hs, err := NewHost(30002)
	if err != nil {
		panic(err)
	}
	if err := Sender(context.Background(), hs, "/ip4/127.0.0.1/tcp/30001/p2p/QmNQuw3ggvzgweanhaT64uBMVaz7cvkKiasGXFt4AUEEce"); err != nil {
		panic(err)
	}
	signal.Notify(ch, os.Interrupt)
	<-ch
}

func NewHost(port int) (host.Host, error) {
	priv, _, err := crypto.GenerateKeyPairWithReader(crypto.RSA, 2048, rand.Reader)
	if err != nil {
		return nil, err
	}
	opts := []libp2p.Option{
		libp2p.ListenAddrStrings(fmt.Sprintf("/ip4/127.0.0.1/tcp/%d", port)),
		libp2p.Identity(priv),
		libp2p.DisableRelay(),
	}
	return libp2p.New(opts...)
}

func Sender(ctx context.Context, hs host.Host, target string) error {
	fmt.Printf("/ip4/127.0.0.1/tcp/30002/p2p/%s \n", hs.ID().Pretty())

	ipfsaddr, err := ma.NewMultiaddr(target)
	if err != nil {
		return err
	}

	pid, err := ipfsaddr.ValueForProtocol(ma.P_IPFS)
	if err != nil {
		return err
	}

	peerid, err := peer.Decode(pid)
	if err != nil {
		return err
	}

	targetPeerAddr, _ := ma.NewMultiaddr(fmt.Sprintf("/ipfs/%s", pid))
	targetAddr := ipfsaddr.Decapsulate(targetPeerAddr)

	hs.Peerstore().AddAddr(peerid, targetAddr, peerstore.PermanentAddrTTL)

	// s 可读可写流
	s, err := hs.NewStream(ctx, peerid, p2p.ID)
	if err != nil {
		return err
	}

	_, err = s.Write([]byte("Hello, world!\n"))
	if err != nil {
		return err
	}
	out, err := ioutil.ReadAll(s)
	if err != nil {
		return err
	}
	fmt.Printf("read reply: %s \n", string(out))
	return nil
}
