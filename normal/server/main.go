/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"bufio"
	"context"
	"crypto/rand"
	"fmt"
	"github.com/libp2p/go-libp2p"
	"github.com/libp2p/go-libp2p-core/crypto"
	"github.com/libp2p/go-libp2p-core/host"
	"github.com/libp2p/go-libp2p-core/network"
	"os"
	"os/signal"
	"x6t.io/p2p"
)

//  B
func main() {
	ch := make(chan os.Signal, 1)
	hs, err := NewHost(30001)
	if err != nil {
		panic(err)
	}
	Listening(context.Background(), hs)
	signal.Notify(ch, os.Interrupt)
	<-ch
}

func NewHost(port int) (host.Host, error) {
	priv, _, err := crypto.GenerateKeyPairWithReader(crypto.RSA, 2048, rand.Reader)
	if err != nil {
		return nil, err
	}
	opts := []libp2p.Option{
		libp2p.ListenAddrStrings(fmt.Sprintf("/ip4/127.0.0.1/tcp/%d", port)),
		libp2p.Identity(priv),
		libp2p.DisableRelay(),
	}
	return libp2p.New(opts...)
}

func Listening(ctx context.Context, hs host.Host) {
	fmt.Printf("/ip4/127.0.0.1/tcp/30001/p2p/%s \n", hs.ID().Pretty())

	hs.SetStreamHandler(p2p.ID, func(stream network.Stream) {
		fmt.Printf("listener received new stream \n")
		buf := bufio.NewReader(stream)
		str, err := buf.ReadString('\n')
		if err != nil {
			fmt.Printf("read buff err: %s", err)
			_ = stream.Reset()
		}
		fmt.Printf("read: %s", str)
		_, err = stream.Write([]byte(str))
		_ = stream.Close()
	})

	fmt.Printf("listening for connections \n")
}
