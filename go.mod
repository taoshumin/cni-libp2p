module x6t.io/p2p

go 1.16

require (
	github.com/libp2p/go-libp2p v0.17.0
	github.com/libp2p/go-libp2p-core v0.13.0
	github.com/libp2p/go-libp2p-swarm v0.9.0
	github.com/multiformats/go-multiaddr v0.4.0
)
